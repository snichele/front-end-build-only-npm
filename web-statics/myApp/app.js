
/*
    Application entry point.
 */
(function(){

    angular.module('myApp', []).controller('myAppCtrl', [function(){
        this.msg = 'Hello from angular controller !';
    }]);

})();