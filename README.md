
Ultra simple front-end dev sample project (with angularjs).

**No bower** : js dependencies are in `/web-statics/vendors` and are added manually (in this demo, in unminified versions ;
feel free to change them with whatever variant you need.)

- Clone the repo
- `npm install`
- Write your code in `/web-statics/` directory
- Watch it live in your browser (with livereload) by running `npm run dev`

Tests modules and karma.conf.js are presents but there is no test demo. Don't try 'npm run test' ;)

Inspired and borrowed from http://blog.keithcirkel.co.uk/how-to-use-npm-as-a-build-tool
